﻿public enum UIView
{
    None = -1,
    Main = 0,
    LevelSelector = 1,
    HUD = 2,
    Settings = 3
}

public enum UIState
{
    Open = 0,
    Close = 1,
    Destroy = 2
}
