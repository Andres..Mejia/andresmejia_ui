﻿using UnityEngine;

public class FactoryView
{
    //Crea modulos segun el Nombre.
    public static UIBase CreateView(UIView view, Transform parent)
    {
        string path = "Views/" + view.ToString();
        GameObject newView = GameObject.Instantiate(Resources.Load(path)) as GameObject;
        if (newView != null)
        {
            newView.transform.parent = parent.transform;
            newView.transform.localPosition = Vector3.zero;
            newView.transform.localScale = Vector3.one;
        }
        return newView.GetComponent<UIBase>();
    }   
}
