﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBase : MonoBehaviour
{
    static Action<bool> action;

    public virtual void OpenView()
    {
        gameObject.SetActive(true);
    }

    protected virtual void CloseView()
    {
        Destroy(this.gameObject);
    }
}
