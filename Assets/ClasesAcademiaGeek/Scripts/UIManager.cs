﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private Transform parentView;
    private Transform settingsView;

    private MainBehaviour main;
    private LevelSelectorBehaviour level;
    private HUDBehaviour hud;
    private SettingsBehaviour settings;

    private void Awake()
    {
        main = GetComponent<MainBehaviour>();
        level = GetComponent<LevelSelectorBehaviour>();
        hud = GetComponent<HUDBehaviour>();
        settings = GetComponent<SettingsBehaviour>();

        parentView = GetComponentInChildren<RectTransform>().transform;
        settingsView = GetComponentInChildren<RectTransform>().transform;

        ChangeView(UIView.Main, UIState.Open);
        SettingsView(UIView.Settings, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        FactoryView.CreateView(view, parentView);
    }

    public void SettingsView(UIView view, UIState viewState)
    {
            FactoryView.CreateView(view, settingsView);
    }
}
